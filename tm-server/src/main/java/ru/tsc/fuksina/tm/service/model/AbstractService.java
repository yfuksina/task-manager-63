package ru.tsc.fuksina.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.fuksina.tm.api.service.model.IService;
import ru.tsc.fuksina.tm.exception.field.IdEmptyException;
import ru.tsc.fuksina.tm.model.AbstractModel;
import ru.tsc.fuksina.tm.repository.model.AbstractRepository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
public abstract class AbstractService<M extends AbstractModel, R extends AbstractRepository<M>> implements IService<M> {

    @NotNull
    protected abstract AbstractRepository<M> getRepository();

    @Override
    @Transactional
    public void add(@NotNull final M model) {
        @NotNull final AbstractRepository<M> repository = getRepository();
        repository.save(model);
    }

    @Override
    @Transactional
    public void set(@NotNull final Collection<M> models) {
        @NotNull final AbstractRepository<M> repository = getRepository();
        for (final M model : models)
            repository.save(model);
    }

    @NotNull
    @Override
    public List<M> findAll() {
        @NotNull final AbstractRepository<M> repository = getRepository();
        return repository.findAll();
    }

    @Nullable
    @Override
    public  M findOneById(@Nullable final String id) {
        Optional.ofNullable(id).filter(item -> !item.isEmpty()).orElseThrow(IdEmptyException::new);
        @NotNull final AbstractRepository<M> repository = getRepository();
        return repository.findById(id).orElse(null);
    }

    @Override
    @Transactional
    public void remove(@NotNull final M model) {
        @NotNull final AbstractRepository<M> repository = getRepository();
        repository.delete(model);
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String id) {
        Optional.ofNullable(id).filter(item -> !item.isEmpty()).orElseThrow(IdEmptyException::new);
        @NotNull final AbstractRepository<M> repository = getRepository();
        repository.deleteById(id);
    }

    @Override
    @Transactional
    public void update(@NotNull final M model) {
        @NotNull final AbstractRepository<M> repository = getRepository();
        repository.save(model);
    }

    @Override
    @Transactional
    public void clear() {
        @NotNull final AbstractRepository<M> repository = getRepository();
        repository.deleteAll();
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        Optional.ofNullable(id).filter(item -> !item.isEmpty()).orElseThrow(IdEmptyException::new);
        @NotNull final AbstractRepository<M> repository = getRepository();
        return repository.existsById(id);
    }

    @Override
    public long getSize() {
        @NotNull final AbstractRepository<M> repository = getRepository();
        return repository.count();
    }

}
