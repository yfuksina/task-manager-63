package ru.tsc.fuksina.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.fuksina.tm.dto.model.TaskDto;
import ru.tsc.fuksina.tm.enumerated.Status;

import java.util.List;

public interface ITaskDtoService extends IUserOwnedServiceDto<TaskDto> {

    @NotNull
    List<TaskDto> findAllByProjectId(@Nullable String userId, @Nullable String projectId);

    @NotNull
    TaskDto changeTaskStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

}
