package ru.tsc.fuksina.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.tsc.fuksina.tm.model.User;

@Repository
public interface UserRepository extends AbstractRepository<User> {

    @Nullable
    User findFirstByLogin(@NotNull final String login);

    @Nullable
    User findFirstByEmail(@NotNull final String email);

}
