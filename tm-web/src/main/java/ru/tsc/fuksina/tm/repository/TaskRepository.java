package ru.tsc.fuksina.tm.repository;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import ru.tsc.fuksina.tm.model.Task;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public class TaskRepository {

    @NotNull
    private static final TaskRepository INSTANCE = new TaskRepository();

    public static TaskRepository getInstance() {
        return INSTANCE;
    }

    @NotNull
    private Map<String, Task> tasks = new LinkedHashMap<>();

    {
       add("Task 1");
       add("Task 2");
       add("Task 3");
    }

    public void add(@NotNull final String name) {
        final Task task = new Task(name);
        tasks.put(task.getId(), task);
    }

    public void removeById(@NotNull final String id) {
        tasks.remove(id);
    }

    @NotNull
    public Collection<Task> findAll() {
        return tasks.values();
    }

    @NotNull
    public Task findById(@NotNull final String id) {
        return tasks.get(id);
    }

    public void save(@NotNull final Task task) {
        tasks.put(task.getId(), task);
    }

}
