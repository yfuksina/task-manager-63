package ru.tsc.fuksina.tm.repository;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import ru.tsc.fuksina.tm.model.Project;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public class ProjectRepository {

    @NotNull
    private static final ProjectRepository INSTANCE = new ProjectRepository();

    public static ProjectRepository getInstance() {
        return INSTANCE;
    }

    @NotNull
    private Map<String, Project> projects = new LinkedHashMap<>();

    {
        add("Project 1");
        add("Project 2");
        add("Project 3");
    }

    public void add(@NotNull final String name) {
        final Project project = new Project(name);
        projects.put(project.getId(), project);
    }

    public void removeById(@NotNull final String id) {
        projects.remove(id);
    }

    @NotNull
    public Collection<Project> findAll() {
        return projects.values();
    }

    @NotNull
    public Project findById(@NotNull final String id) {
        return projects.get(id);
    }

    public void save(@NotNull final Project project) {
        projects.put(project.getId(), project);
    }

}
