package ru.tsc.fuksina.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.fuksina.tm.event.ConsoleEvent;
import ru.tsc.fuksina.tm.listener.AbstractListener;

import java.util.Collection;

@Component
public final class CommandsListener extends AbstractSystemListListener {

    @NotNull
    public static final String NAME = "command";

    @NotNull
    public static final String DESCRIPTION = "Show application commands";

    @NotNull
    public static final String ARGUMENT = "-cmd";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    @EventListener(condition = "@commandsListener.getName() == #event.name || @commandsListener.getArgument() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        @NotNull final Collection<AbstractListener> commands = getCommands();
        for (final AbstractListener command : commands) {
            @Nullable final String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }

}
